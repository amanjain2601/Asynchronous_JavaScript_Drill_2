const cardsData = require("./cards.json");

function findData(listID, callback) {
  if (typeof listID != "string") {
    callback(new Error("Id is not of string type"));
  } else if (cardsData[listID] == undefined) {
    callback(new Error("Invalid id"));
  } else {
    callback(null, cardsData[listID]);
  }
}

function findCardsDetails(listID, callback) {
  setTimeout(findData, 2 * 1000, listID, callback);
}

module.exports = findCardsDetails;
