const boardsReturnedFunction = require("./callback1.js");
const listReturnedFunction = require("./callback2.js");
const cardsReturnedFunction = require("./callback3.js");

let boardsData = require("./boards.json");

function FindThanosCardInfo(boardsByName, callback) {
  setTimeout(() => {
    let boardID = boardsData.find((current) => {
      return current.name == boardsByName;
    });

    // check if board id is valid or not
    if (boardID == undefined) {
      callback(new Error("Invalid name or input"));
    } else {
      boardsReturnedFunction(boardID.id, callbackOfBoard);

      function callbackOfBoard(err, data) {
        if (err) {
          callback(new Error("Invalid input"));
        } else {
          callback(null, data);
          // Find all lists for the board id of Thanos
          listReturnedFunction(data.id, callbackOfList);

          function callbackOfList(err, data) {
            if (err) {
              callback(new Error("Invalid input"));
            } else {
              callback(null, data);

              // Find id of mind from lists of Thanos board id
              let MindID = data.find((current) => {
                return current.name == "Mind";
              });

              // Find id of space from lists of Thanos board id
              let SpaceID = data.find((current) => {
                return current.name == "Space";
              });

              cardsReturnedFunction(MindID.id, callbackOfCards);
              cardsReturnedFunction(SpaceID.id, callbackOfCards);

              function callbackOfCards(err, data) {
                if (err) {
                  callback(new Error("Invalid id for Mind"));
                } else {
                  callback(null, data);
                }
              }
            }
          }
        }
      }
    }
  }, 2000);
}

module.exports = FindThanosCardInfo;
