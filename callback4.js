const boardsReturnedFunction = require("./callback1.js");
const listReturnedFunction = require("./callback2.js");
const cardsReturnedFunction = require("./callback3.js");

const boardsData = require("./boards.json");

function FindThanosInformation(boardsByName, callback) {
  // Search if name is Valid Name
  setTimeout(() => {
    let findIDByName = boardsData.find((current) => {
      return current.name == boardsByName;
    });

    if (findIDByName == undefined) {
      callback(new Error("sorry Invalid name"));
    } else {
      // get the information of id of the Name in boards by calling function
      boardsReturnedFunction(findIDByName.id, callbackOfBoard);

      function callbackOfBoard(err, data) {
        if (err) {
          callback(new Error("Id is invalid"));
        } else {
          callback(null, data);
          // get the lists of all id by calling function
          listReturnedFunction(findIDByName.id, callbackOfList);

          function callbackOfList(err, data) {
            if (err) {
              callback(new Error("Id is Invalid"));
            } else {
              callback(null, data);
              // search id of mind
              let idOfThanosByMind = data.find((current) => {
                return current.name == "Mind";
              });

              if (idOfThanosByMind == undefined) {
                callback(new Error("Invalid id"));
              } else {
                cardsReturnedFunction(idOfThanosByMind.id, callbackOfCards);

                function callbackOfCards(err, data) {
                  if (err) {
                    callback(new Error("Invalid id"));
                  } else {
                    callback(null, data);
                  }
                }
              }
            }
          }
        }
      }
    }
  }, 2 * 1000);
}

module.exports = FindThanosInformation;
