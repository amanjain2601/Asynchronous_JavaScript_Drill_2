const boardsReturnedFunction = require("./callback1.js");
const listReturnedFunction = require("./callback2.js");
const cardsReturnedFunction = require("./callback3.js");

const cardsData = require("./cards.json");
let boardsData = require("./boards.json");

function FunctionGetAllCards(boardName, callback) {
  setTimeout(() => {
    let boardID = boardsData.find((current) => {
      return current.name == boardName;
    });

    // check if its valid board id
    if (boardID == undefined) {
      callback(new Error("Invalid input"));
    } else {
      boardsReturnedFunction(boardID.id, callbackOfBoards);

      function callbackOfBoards(err, data) {
        if (err) {
          callback(new Error("Invalid id"));
        } else {
          callback(null, data);
          // get lists for a particular board id
          listReturnedFunction(data.id, callbackOfLists);

          function callbackOfLists(err, data) {
            if (err) {
              callback(new Error("Invalid input"));
            } else {
              callback(null, data);

              // get cards for all lists
              data.forEach((current) => {
                cardsReturnedFunction(current.id, callbackOfCards);

                function callbackOfCards(err, data) {
                  if (err) {
                    callback(null, "Id is not there in Cards");
                  } else {
                    callback(null, data);
                  }
                }
              });
            }
          }
        }
      }
    }
  }, 2000);
}

module.exports = FunctionGetAllCards;
