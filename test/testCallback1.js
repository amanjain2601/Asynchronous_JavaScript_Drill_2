const returnedFunction = require("../callback1.js");

function callback(err, data) {
  if (err) {
    console.log(err);
  } else {
    if (
      JSON.stringify(data) ===
      JSON.stringify({
        id: "mcu453ed",
        name: "Thanos",
        permissions: {},
      })
    ) {
      console.log("Your data is", data);
      console.log("Sucess");
    } else {
      console.log("Failed");
    }
  }
}

returnedFunction("mcu453ed", callback);
