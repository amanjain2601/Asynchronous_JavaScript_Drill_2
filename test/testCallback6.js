const returnedFunction = require("../callback6.js");
let count = 0;

function callback(err, data) {
  if (err) {
    console.log(err);
  } else {
    console.log(data);
    if (
      count == 0 &&
      JSON.stringify(data) ==
        JSON.stringify({
          id: "mcu453ed",
          name: "Thanos",
          permissions: {},
        })
    ) {
      count++;
    } else if (
      count == 1 &&
      JSON.stringify(data) ==
        JSON.stringify([
          {
            id: "qwsa221",
            name: "Mind",
          },
          {
            id: "jwkh245",
            name: "Space",
          },
          {
            id: "azxs123",
            name: "Soul",
          },
          {
            id: "cffv432",
            name: "Time",
          },
          {
            id: "ghnb768",
            name: "Power",
          },
          {
            id: "isks839",
            name: "Reality",
          },
        ])
    ) {
      count++;
    } else if (
      count == 2 &&
      JSON.stringify(data) ==
        JSON.stringify([
          {
            id: "1",
            description:
              "Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar",
          },
          {
            id: "2",
            description:
              "Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar",
          },
          {
            id: "3",
            description:
              "Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar",
          },
        ])
    ) {
      count++;
    } else if (
      count == 3 &&
      JSON.stringify(data) ==
        JSON.stringify([
          {
            id: "1",
            description:
              "intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.",
          },
          {
            id: "2",
            description:
              "intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.",
          },
          {
            id: "3",
            description:
              "intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.",
          },
          {
            id: "4",
            description:
              "intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.",
          },
        ])
    ) {
      count++;
    } else if (
      count == 4 &&
      JSON.stringify(data) ==
        JSON.stringify([
          {
            id: "1",
            description:
              "intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.",
          },
          {
            id: "2",
            description:
              "intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.",
          },
        ])
    ) {
      count++;
    } else if (
      count == 5 &&
      JSON.stringify(data) ==
        JSON.stringify([
          {
            id: "1",
            description:
              "intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.",
          },
          {
            id: "2",
            description:
              "intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.",
          },
        ])
    ) {
      count++;
    } else if (
      count == 6 &&
      JSON.stringify(data) ==
        JSON.stringify([
          {
            id: "1",
            description:
              "intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.",
          },
          {
            id: "2",
            description:
              "intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.",
          },
        ])
    ) {
      count++;
    } else if (count == 7 && data == "Id is not there") {
      console.log("Success");
    }
  }
}

returnedFunction("Thanos", callback);
