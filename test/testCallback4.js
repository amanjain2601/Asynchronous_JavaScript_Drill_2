const returnedFunction = require("../callback4.js");

let count = 0;
returnedFunction("Thanos", callback);
returnedFunction(67, callback);
returnedFunction("ggigi", callback);

function callback(err, data) {
  if (err) {
    console.log(err);
  } else {
    console.log(data);
    if (
      count == 0 &&
      JSON.stringify(data) ==
        JSON.stringify({
          id: "mcu453ed",
          name: "Thanos",
          permissions: {},
        })
    )
      count++;
    else if (
      count == 1 &&
      JSON.stringify(data) ==
        JSON.stringify([
          {
            id: "qwsa221",
            name: "Mind",
          },
          {
            id: "jwkh245",
            name: "Space",
          },
          {
            id: "azxs123",
            name: "Soul",
          },
          {
            id: "cffv432",
            name: "Time",
          },
          {
            id: "ghnb768",
            name: "Power",
          },
          {
            id: "isks839",
            name: "Reality",
          },
        ])
    )
      count++;
    else if (
      count == 2 &&
      JSON.stringify(data) ==
        JSON.stringify([
          {
            id: "1",
            description:
              "Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar",
          },
          {
            id: "2",
            description:
              "Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar",
          },
          {
            id: "3",
            description:
              "Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar",
          },
        ])
    )
      console.log("Success");
  }
}
