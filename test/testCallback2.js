const returnedFunction = require("../callback2.js");

function callback(err, data) {
  if (err) {
    console.log(err);
  } else {
    if (
      JSON.stringify(data) ==
      JSON.stringify([
        { id: "qwsa221", name: "Mind" },
        { id: "jwkh245", name: "Space" },
        { id: "azxs123", name: "Soul" },
        { id: "cffv432", name: "Time" },
        { id: "ghnb768", name: "Power" },
        { id: "isks839", name: "Reality" },
      ])
    ) {
      console.log("Your data is", data);
      console.log("Success");
    } else {
      console.log("Failed");
    }
  }
}

returnedFunction("mcu453ed", callback);
