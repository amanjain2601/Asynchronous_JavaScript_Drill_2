const returnedFunction = require("../callback3.js");

function callback(err, data) {
  if (err) {
    console.log(err);
  } else {
    if (
      JSON.stringify(data) ==
      JSON.stringify([
        {
          id: "1",
          description:
            "intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.",
        },
        {
          id: "2",
          description:
            "intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.",
        },
      ])
    ) {
      console.log("Your data is", data);
      console.log("Success");
    } else {
      console.log("Failed");
    }
  }
}

returnedFunction("azxs123", callback);
