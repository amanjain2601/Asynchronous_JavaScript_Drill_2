const boardsData = require("./boards.json");

function findData(callback, boardsID) {
  if (typeof boardsID != "string") {
    callback(new Error("Id is not of type string"));
  } else {
    let answer = boardsData.find((current) => {
      return current.id == boardsID;
    });
    callback(null, answer);
  }
}

function findDataByBoardID(boardsID, callback) {
  setTimeout(findData, 2000, callback, boardsID);
}

module.exports = findDataByBoardID;
