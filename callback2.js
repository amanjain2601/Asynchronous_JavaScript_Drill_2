let ListsData = require("./lists.json");

function findData(boardsID, callback) {
  if (typeof boardsID != "string") {
    callback(new Error("Id is not of type string"));
  } else if (ListsData[boardsID] == undefined) {
    callback(new Error("Invalid id"));
  } else {
    callback(null, ListsData[boardsID]);
  }
}

function findListDetails(boardsID, callback) {
  setTimeout(findData, 2 * 1000, boardsID, callback);
}

module.exports = findListDetails;
